// This is your test secret API key.
let stripe = require('stripe')
const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.static('.'));


if (process.env.NODE_ENV === 'development') {
  stripe = stripe('sk_test_51KmRo4AqW2c5qowMjHTWi9EYo9R0dVRDIpI7W2ZMilWR884nkaVRw6dqfOHYsdmRaNDJZxuSMkj0XJPMEic1y0ql00aVvDnIVt');
} else {
  stripe = stripe('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');
}

const YOUR_DOMAIN = 'https://buy-bud.web.app';

app.post('/create-checkout-session', async (req, res) => {
  const orderType = req.query.orderType
  let productDataName = ''
  let productAmount = 0
  if (orderType === 'specificPerson') {
    productDataName = 'Specific Person'
    productAmount = 10000 * 100
  } else if (orderType === 'generalPerson') {
    productDataName = 'General Person'
    productAmount = 500 * 100
  }

  const session = await stripe.checkout.sessions.create({
    line_items: [
      {
        // Provide the exact Price ID (for example, pr_1234) of the product you want to sell
        price_data: {
          currency: 'USD',
          product_data: {
            name: `${productDataName} - First Impression`
          },
          unit_amount: productAmount,
          tax_behavior: 'inclusive'
        },
        quantity: 1
      }
    ],
    mode: 'payment',
    success_url: `${YOUR_DOMAIN}/success.html`,
    cancel_url: `${YOUR_DOMAIN}/cancel.html`,
    automatic_tax: {enabled: true},
  });

  res.redirect(303, session.url);
});

const port = process.env.PORT || 4242
app.listen(port, () => console.log('Running on port ', port));
